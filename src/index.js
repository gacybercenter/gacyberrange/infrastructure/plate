import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './Login';
import MainPage from './MainPage';

ReactDOM.render(
  <React.StrictMode>
    <Login />
    {/* Commenting out until I figure out React router */}
    {/* <MainPage /> */}
  </React.StrictMode>,
  document.getElementById('root')
);
